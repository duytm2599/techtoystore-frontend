import { Navigate } from "react-router-dom";

interface Props {
  children: JSX.Element;
  isAllow: boolean;
  user: { id: string; username: string; roles: { id: string; name: string } };
}

const ProtectedRoute = ({ children, isAllow, user }: Props) => {
  if (!user) {
    return <Navigate to="/" />;
  }

  if (isAllow) {
    return children;
  }

  return <Navigate to="/404" />;
};

export default ProtectedRoute;
